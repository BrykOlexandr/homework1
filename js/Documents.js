/**
 * Represents document class
 * @param {string} name defines title of the document
 * @param {number} numPages defines number of pages of the document
 * @constructor
 */
function Documents(name, numPages) {
    if (typeof name === 'string' && name.length > 0) this.name = name;
    if (typeof numPages === 'number' && numPages > 0) this.numPages = numPages;
}