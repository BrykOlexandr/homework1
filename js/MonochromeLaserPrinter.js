/**
 * Represents Monochrome Laser Printer Class
 * @param {string} brand defines printer manufacturer brand
 * @param {string} model defines model of printer
 * @param {printerSupplies} printerSuppliesLimit defines maximum limits for the printer supplies
 * @constructor
 */
function MonochromeLaserPrinter(brand, model, printerSuppliesLimit) {
    this.brand = brand;
    this.model = model;
    var printerSupplies = new PrinterSupplies(0, 0, 0, 0, printerSuppliesLimit);

    /**
     * Print printer information
     */
    this.printerInfo = function () {
        console.log('Printer made by: ' + this.brand);
        console.log('Printer model: ' + this.model);
    };

    /**
     * Add printer supplies by name
     * @param {string} name defines printer supplies name
     * @param {number} value defines printer supplies value
     */
    this.addPrinterSupplies = function (name, value) {
        printerSupplies.increase(name, value);
    };

    /**
     * Change set value of printer supplies on a given
     * @param {string} name defines printer supplies name
     * @param {number} remainder defines printer supplies value
     */
    this.changeToPrinterSupplies = function (name, remainder) {
        printerSupplies.clear(name, remainder);
    };

    /**
     * Print document
     * @param {string} paperType defines paper type. Valid values: A3, A4, A5.
     * @param {Documents} doc defines document that should be printed
     * @param {string} printType defines print type. Valid values: ALL, EVENPAGES, ODDPAGES.
     * @returns {Print}
     */
    this.printDoc = function (paperType, doc, printType) {

        var print = new Print(paperType, doc, printType);

        var supplies = print.getPrinterSupplies();
        for (var name in supplies) {
            if (supplies[name] > 0) {
                printerSupplies.decrease(name, supplies[name]);
            }
        }

        return print;
    }
}