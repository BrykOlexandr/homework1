var printerSuppliesLimit = new PrinterSupplies(170, 170, 170, 700);
var Canon = new MonochromeLaserPrinter('Canon', 'LBP6000', printerSuppliesLimit);
var Serg = new User('Serg');

Canon.printerInfo();
Canon.addPrinterSupplies('A4', 170);
Canon.addPrinterSupplies('toner', 700);

Serg.connectPrinter(Canon);
Serg.computerOn(true);
Serg.printDocument('A4', 'Dune', 'ALL');
Serg.printDocument('A4', 'Dune', 'EVENPAGES');
Serg.printDocument('A4', 'Dune', 'ODDPAGES');

Canon.changeToPrinterSupplies('A4', 0);
Canon.addPrinterSupplies('A5', 10);

Serg.printDocument('A5', 'act', 'ALL');