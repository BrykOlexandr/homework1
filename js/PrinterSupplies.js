/**
 * Represents printer supplies class
 * @param {number} paperA3 defines number of A3 sheets
 * @param {number} paperA4 defines number of A4 sheets
 * @param {number} paperA5 defines number of A5 sheets
 * @param {number} toner defines amount of toner
 * @param {PrinterSupplies} printerSuppliesLimit defines limits for each printer supplies
 * @constructor
 */
function PrinterSupplies(paperA3, paperA4, paperA5, toner, printerSuppliesLimit) {

    if (paperA3 >= 0 && paperA4 >= 0 && paperA5 >= 0 && toner >= 0) {
        this.A3 = +paperA3;
        this.A4 = +paperA4;
        this.A5 = +paperA5;
        this.toner = +toner;
    } else {
        throw new Error('Please enter only integer value of toner and paper!');
    }

    /**
     * Add printer supplies by name
     * @param {string} name defines printer supplies name
     * @param {number} value defines printer supplies value
     */
    this.increase = function (name, value) {
        if (name in this) {
            if (value < 0) {
                throw new Error('Value must be more than zero or zero!');
            }

            if (printerSuppliesLimit && printerSuppliesLimit[name] && (this[name] + value) > printerSuppliesLimit[name]) {
                var delta = this[name] + value - printerSuppliesLimit[name];
                throw new Error('Too large value ' + name + ', decrease to ' + delta + '!');
            }

            this[name] += value;

        } else {
            throw new Error('Unknown: ' + name);
        }
    };

    /**
     * Subtract printer supplies by name
     * @param {string} name defines printer supplies name
     * @param {number} value defines printer supplies value
     */
    this.decrease = function (name, value) {
        if (name in this) {
            if (value < 0) {
                throw new Error('Value must be more than zero or zero!');
            }

            if (printerSuppliesLimit && printerSuppliesLimit[name] && (this[name] - value) < 0) {
                var delta = value - this[name];
                throw {
                    message: 'Not enough ' + name + ', in an amount of ' + delta + '!',
                    suppliesName: name,
                    delta: delta
                };
            }

            this[name] -= value;

        } else {
            throw new Error('Unknown: ' + name);
        }
    };

    /**
     * Change set value of printer supplies on a given
     * @param {string} name defines printer supplies name
     * @param {number} remainder defines printer supplies new value
     */
    this.clear = function (name, remainder) {
        if (name in this) {
            if (remainder < 0 && remainder > this[name]) {
                throw new Error('Invalid value!');
            }

            this[name] = remainder;

        } else {
            throw new Error('Unknown: ' + name);
        }
    };
}