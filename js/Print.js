var Print = (function () {
    var TONERUSAGE = {
        A3: 4,
        A4: 2,
        A5: 1
    };

    /**
     * Represents print class
     * @param {string} paperType defines paper type. Valid values: A3, A4, A5.
     * @param {Documents} doc defines document that should be printed
     * @param {string} printType defines print type. Valid values: ALL, EVENPAGES, ODDPAGES.
     * @constructor
     */
    function Print(paperType, doc, printType) {
        var printerSupplies;
        var tonerUsage;
        var numToner;
        var numPages;
        var a3 = 0, a4 = 0, a5 =0;

        if (typeof paperType != 'string' || paperType == '') {
            throw new Error('Invalid value of paper size: ' + paperType + '!');
        }
        if (typeof doc != 'object' || !('name' in doc) || !('numPages' in doc)) {
            throw new Error('This is not the document!');
        }
        if (typeof printType != 'string' || printType == '') {
            throw new Error('Incorrect type of printing: ' + printType + '!');
        }

        switch (paperType) {
            case 'A3':
                tonerUsage = TONERUSAGE.A3;
                a3 = doc.numPages;
                break;
            case 'A4':
                tonerUsage = TONERUSAGE.A4;
                a4 = doc.numPages;
                break;
            case 'A5':
                tonerUsage = TONERUSAGE.A5;
                a5 = doc.numPages;
                break;
            default:
                throw new Error('Printer does not support  this paper size!');
        }


        if (printType == 'ALL') {
            numPages = doc.numPages;
            numToner = tonerUsage * doc.numPages;
            printerSupplies = new PrinterSupplies(a3, a4, a5, numToner);
        } else if (printType == 'ODDPAGES') {
            numPages = Math.ceil(doc.numPages / 2);
            numToner = tonerUsage * numPages;
            a3 = a3 ? numPages : a3;
            a4 = a4 ? numPages : a4;
            a5 = a5 ? numPages : a5;
            printerSupplies = new PrinterSupplies(a3, a4, a5, numToner);
        } else if (printType == 'EVENPAGES') {
            numPages = Math.floor(doc.numPages / 2);
            numToner = tonerUsage * numPages;
            a3 = a3 ? numPages : a3;
            a4 = a4 ? numPages : a4;
            a5 = a5 ? numPages : a5;
            printerSupplies = new PrinterSupplies(a3, a4, a5, numToner);
        } else {
            /**
             @TODO implement other options such: "1,4,9", "2-5"
             */
            throw new Error('Enter the correct information about printing!');
        }

        /**
         * Returns required printer supplies
         * @returns {PrinterSupplies}
         */
        this.getPrinterSupplies = function () {
            return printerSupplies;
        };

        /**
         * Returns number of printed pages
         * @returns {number}
         */
        this.getInfo = function () {
            return numPages;
        }
    }

    return Print;
})();