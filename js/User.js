/**
 * Represents printer user class
 * @param {string} nickname defines user nickname
 * @constructor
 */
function User(nickname) {
    if (typeof nickname === 'string' && nickname.length > 0) {
        this.nickname = nickname;
    } else {
        throw new Error('NickName must be a string');
    }
    var printer = null;
    var isOn = null;
    var doc;

    /**
     * Connect printer to your computer
     * @param {MonochromeLaserPrinter} printers defines printer which connect
     */
    this.connectPrinter = function(printers) {
        printer = printers;
    };

    /**
     * Turns on the user's computer
     * @param {boolean} status defines the computer is turned on. TRUE - yes, FALSE - no.
     */
    this.computerOn = function(status) {
        isOn = status;
    };

    /**
     * Print document
     * @param {string} paperType defines paper type. Valid values: A3, A4, A5.
     * @param {string} docName defines title of the document you need print
     * @param {string} printType defines print type. Valid values: ALL, EVENPAGES, ODDPAGES.
     */
    this.printDocument = function(paperType, docName, printType) {
        if(!isOn) {
            throw new Error('Initially turn on the computer!');
        }

        if(!printer) {
            throw new Error('Printer are not connected to the computer!');
        }

        /* Put in {object}doc all available documents for printing */
        $.ajax({
            url: 'data.json', //all available files on your computer
            method: 'GET',
            async: false,
            success: function(result){
                doc = result;
            },
            error: function(e){
                console.error(e)
            }
        });

        if(!docName in doc) {
            throw new Error('Document with the same name not available on computer!');
        }

        var pages = doc[docName];
        var document = new Documents(docName, pages);

        try {
            var print = printer.printDoc(paperType, document, printType);
            console.log(this.nickname + ' printed on printer ' + printer.brand + ' model ' + printer.model + ' documents "' + docName + '" ' + print.getInfo() + ' pages. Format: ' + paperType + '.');
        } catch (e) {
            if (e.suppliesName) {
                console.error(e.message);
                printer.addPrinterSupplies(e.suppliesName, e.delta);
                console.log(this.nickname + ' added ' + e.suppliesName+ ' in an amount of ' + e.delta+ '.');
                this.printDocument(paperType, docName, printType);
            } else {
                alert('When working Printers error occurred. Contact the service center!');
                console.error(e);
            }
        }
    }
}